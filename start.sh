#!/bin/bash
echo "Start"

if [ -z "$1" ]
then
    echo "OPENAI_API_KEY parameter is empty";
    exit 1;
fi
echo "git pull origin develop"
git pull origin develop

echo "Delete image greetings"
sudo docker rmi greetings

echo "Build image greetings"
sudo docker build . -t greetings

echo "Run image greetings"
sudo docker run --rm -p 3000:3000 -e PORT=3000 -e OPENAI_API_KEY=$1 greetings