import { create, start } from './server.js';

const startServer = async () => {
    const appCreate = await create();
    await start(appCreate);
};

startServer();
