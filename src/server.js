import * as fs from 'fs';
import { port } from './config.js';


import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import { pinoHttp } from 'pino-http';
import swaggerUi from 'swagger-ui-express';
import YAML from 'yaml';
import { getAllModels } from './controllers/model.js';
import { getCompletions } from './controllers/completions.js';
import { getChat } from './controllers/chat.js';
import { getEdits } from './controllers/edits.js';
import { generateImages, editImages, variationImages, addTextToImage } from './controllers/images.js';

const file = fs.readFileSync('src/swagger.yml', 'utf8');
const swaggerDocument = YAML.parse(file);



const app = express();
const uncaughtException = async () => {
    process.on('uncaughtException', (err) => {
        console.error('uncaughtException');
        console.error(err);
    });
};

export const create = async () => {
    app.use(cors());
    app.use(pinoHttp({ level: 'error' }));
    app.use(
        bodyParser.urlencoded({
            extended: true,
        }),
    );
    app.use(bodyParser.json());

    app.get('/', (req, res) => {
        res.send('Ok');
    });

    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    app.get('/api/models', async (req, res) => res.json(await getAllModels()));

    app.post('/api/completions', async (req, res) => {
        try {
            const { prompt } = req.body;
            const result = await getCompletions({ prompt });
            res.json(result);
        } catch (error) {
            res.status(500).send(String(error));
        }
    });

    app.post('/api/chat/completions', async (req, res) => {
        try {
            const { messages } = req.body;
            const result = await getChat({ messages });
            res.json(result);
        } catch (error) {
            res.status(500).send(String(error));
        }
    });

    app.post('/api/edits', async (req, res) => {
        try {
            const { input, instruction } = req.body;
            const result = await getEdits({ input, instruction });
            res.json(result);
        } catch (error) {
            res.status(500).send(String(error));
        }
    });

    app.post('/api/images/generations', async (req, res) => {
        try {
            const { prompt, n, size } = req.body;
            const result = await generateImages({ prompt, n, size });
            res.json(result);
        } catch (error) {
            res.status(500).send(String(error));
        }
    });

    app.post('/api/images/edits', async (req, res) => {
        try {
            const parameters = { ...req.body };
            const result = await editImages(parameters);
            res.json(result);
        } catch (error) {
            res.status(500).send(String(error));
        }
    });

    app.post('/api/images/variations', async (req, res) => {
        try {
            const parameters = { ...req.body };
            const result = await variationImages(parameters);
            res.json(result);
        } catch (error) {
            res.status(500).send(String(error));
        }
    });

    app.post('/api/images/text', async (req, res) => {
        try {
            const parameters = { ...req.body };
            const result = await addTextToImage(parameters);
            res.json(result);
        } catch (error) {
            res.status(500).send(String(error));
        }
    });

    uncaughtException();
    return app;
};

export const start = async (app) => {
    await new Promise((res) => {
        app.listen(port, () => {
            console.log(`Server started on port ${port}`);
            res();
        });
    });
    uncaughtException();
};
