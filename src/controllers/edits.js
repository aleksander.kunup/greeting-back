import { openai } from '../config.js';

export const getEdits = async ({ input, instruction }) => {
    try {
        const response = await openai.createEdit({
            model: 'text-davinci-edit-001',
            input,
            instruction,
            temperature: 0.7,
        });
        console.log(input, instruction);
        console.log(response.data);
        return response.data.choices.map((choice) => ({ text: choice.text, index: choice.index }));
    } catch (error) {
        console.error(error);
        throw new Error(error?.response?.data?.error?.message || error?.message);
    }
};
