import { openai } from '../config.js';

export const getAllModels = async () => {
    try {
        const response = await openai.listModels();
        console.log(Object.keys(response.data.data[0]));
        return response.data.data.map((model) => ({ model: model.id }));
    } catch (error) {
        console.error(error);
        throw new Error(error?.response?.data?.error?.message || error?.message);
    }
};
