import { openai } from '../config.js';

export const getChat = async ({ messages }) => {
    try {
        const completion = await openai.createChatCompletion({
            model: 'gpt-4',
            messages,
            max_tokens: 1000,
            temperature: 0.7,
        });
        console.log(messages);
        console.log(completion.data);
        return completion.data.choices.map((choice) => ({ message: choice.message, index: choice.index }));
    } catch (error) {
        console.error(error);
        throw new Error(error?.response?.data?.error?.message || error?.message);
    }
};
