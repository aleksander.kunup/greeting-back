import { openai } from '../config.js';

export const getCompletions = async ({ prompt }) => {
    try {
        const response = await openai.createCompletion({
            model: 'text-davinci-003',
            prompt,
            max_tokens: 1000,
            temperature: 0.7,
        });
        //console.log(prompt);
        //console.log(response.data);
        return response.data.choices.map((choice) => ({ text: choice.text, index: choice.index }));
    } catch (error) {
        console.error(error);
        throw new Error(error?.response?.data?.error?.message || error?.message);
    }
};
