import { describe, expect, it } from '@jest/globals';

describe('getChat', () => {

    it('calls openai.createChatCompletion with the correct arguments', () => {
        expect((1 + 2)).toBe(3);
    });
    it('returns an array of objects with message and index properties', () => {
        expect((1 + 2)).toBe(3);
    });
    it('throws an error if openai.createChatCompletion throws an error', () => {
        expect((1 + 2)).toBe(3);
    });
});

describe('getCompletions', () => {

    it('should return an array of choices', () => {
        expect((1 + 2)).toBe(3);
    });
    it('should throw an error if request fails', () => {
        expect((1 + 2)).toBe(3);
    });
    it('throws an error if openai.createCompletion throws an error', () => {
        expect((1 + 2)).toBe(3);
    });
});
