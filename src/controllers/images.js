import { openai } from '../config.js';
import fs from 'node:fs';
import sharp from 'sharp';
import axios from 'axios';
import * as stream from 'node:stream';
import { promisify } from 'node:util';
import crypto from 'node:crypto';
import nodeHtmlToImage from 'node-html-to-image';

const sizes = ['256x256', '512x512', '1024x1024'];


export const generateImages = async ({ prompt, n = 1, size = '1024x1024' }) => {
    try {
        const response = await openai.createImage({ prompt, n, size });
        console.log(prompt);
        console.log(response.data.data);
        return response.data.data;
    } catch (error) {
        console.error(error);
        throw new Error(error?.response?.data?.error?.message || error.message);
    }
};

const finished = promisify(stream.finished);

const downloadFile = async (url, outputLocationPath, width = 1024, height = 1024) => new Promise((res, rej) => {
    const writer = fs.createWriteStream(outputLocationPath);
    axios({ method: 'get', url, responseType: 'stream' })
        .then(async (response) => {
            response.data
                .pipe(sharp().toFormat('png').resize({
                    width,
                    height,
                }).ensureAlpha())
                .pipe(writer);
            await finished(writer);
            res();
        })
        .catch((error) => rej(error));
});

const transparentAllImageBuffer = async (width, height) => await sharp({
    create: {
        width,
        height,
        channels: 4,
        background: { r: 0, g: 0, b: 0, alpha: 0 }
    }
})
    .png()
    .toBuffer();

const fillImageBuffer = async (width, height) => await sharp({
    create: {
        width,
        height,
        channels: 4,
        background: { r: 0, g: 0, b: 0, alpha: 1 }
    }
})
    .png()
    .toBuffer();

const deleteImages = (images) => {
    images.forEach((image) => {
        fs.unlink(image, (error) => {
            if (error) {
                console.log('Delete image error');
                console.error(error);
            }
        });
    });
};

const editedAreas = ['bottom', 'middle', 'top', 'full'];

export const editImages = async (parameters) => {
    try {
        const { image, prompt, n = 1 } = parameters;
        let { size, editedArea } = parameters;

        if (!sizes.includes(size)) size = '1024x1024';
        if (!editedAreas.includes(editedArea)) editedArea = editedAreas[0];

        const [ width, height ] = size.split('x').map((item) => +item);

        const filledArea = {
            bottom: { filledWidth: width, filledHeight: Math.floor(height * (2 / 3)), left: 0, top: 0 },
            top:  {
                filledWidth: width,
                filledHeight: Math.floor(height * (2 / 3)),
                left: 0,
                top: Math.floor(height * (1 / 3))
            },
            full: { filledWidth: width, filledHeight: 10, left: 0, top: 0 },
            middle: { filledWidth: width, filledHeight: Math.floor(height * (1 / 3)), left: 0, top: 0 }
        };
        const transparentImage = await transparentAllImageBuffer(width, height);
        filledArea[editedArea].filledWidth;
        filledArea[editedArea].filledHeight;
        const fillEmptyImage = await fillImageBuffer(
            filledArea[editedArea].filledWidth,
            filledArea[editedArea].filledHeight
        );


        const imagePath = `${crypto.randomUUID()}.png`;
        const maskPath = `${crypto.randomUUID()}-mask.png`;
        const maskPathForMiddle = `${crypto.randomUUID()}-mask.png`;
        await downloadFile(image, imagePath, width, height);
        console.log(imagePath);
        console.log(prompt);

        await sharp(transparentImage)
            .composite([{ input: fillEmptyImage, left: 0, top: filledArea[editedArea].top }])
            .toFile(maskPath);

        if (editedArea === 'middle') {
            await sharp(maskPath)
                .composite([{ input: fillEmptyImage, left: 0, top: Math.floor(height * (2 / 3)) }])
                .toFile(maskPathForMiddle);
        }

        const response = await openai.createImageEdit(
            fs.createReadStream(imagePath),
            prompt,
            fs.createReadStream(editedArea === 'middle' ? maskPathForMiddle : maskPath),
            n,
            size
        );
        console.log(response.data.data);
        deleteImages([imagePath, maskPath]);
        if (editedArea === 'middle') deleteImages([maskPathForMiddle]);
        return response.data.data;
    } catch (error) {
        console.error(error);
        console.log(Object.keys(error));
        console.dir(error.response?.status);
        console.dir(error.response?.statusText);
        console.dir(error.response?.data?.error?.message);
        throw new Error(error?.response?.data?.error?.message || error.message);
    }
};

export const variationImages = async (parameters) => {
    try {
        const { image, n } = parameters;
        let { size } = parameters;
        if (!sizes.includes(size)) size = '1024x1024';
        const [ width, height ] = size.split('x').map((item) => +item);
        const imagePath = crypto.randomUUID() + '.png';
        await downloadFile(image, imagePath, width, height);
        console.log(imagePath);
        const response = await openai.createImageVariation(
            fs.createReadStream(imagePath),
            n,
            size
        );
        console.log(response.data.data);
        deleteImages([imagePath]);
        return response.data.data;
    } catch (error) {
        console.error(error);
        throw new Error(error?.response?.data?.error?.message || error.message);
    }
};

export const addTextToImage = async (parameters) => {
    try {
        const { image, text } = parameters;
        let { fontSize, color, top } = parameters;
        fontSize = fontSize || '40px';
        color = color || 'red';
        top = top || '0px';
        const textHTML = `
        <div style="border-right: 30px solid transparent">
            <div style="
                font-size: ${fontSize};
                color: ${color};
                text-align: center;
                word-wrap: break-word;
                position: absolute;
                top: ${top};
                right: 10px;
                border: 10px solid transparent;
            ">
                ${text}
            </div>
        </div>`;

        const imagePath = `${crypto.randomUUID()}.png`;
        await downloadFile(image, imagePath);

        const tempTextImage = `${crypto.randomUUID()}-temp.png`;
        const outputImage = `${crypto.randomUUID()}-out.png`;

        await nodeHtmlToImage({
            output: tempTextImage,
            html: textHTML,
            transparent: true,
            puppeteerArgs: { args: ['--no-sandbox'] }, // Add this line if running on a server
        });

        // Overlay the text image onto the original image
        await sharp(imagePath)
            .composite([{ input: tempTextImage, blend: 'over' }])
            .toFile(outputImage);

        const response = await openai.createImageEdit(
            fs.createReadStream(outputImage),
            'Do not change'
        );
        console.log(response.data.data);
        deleteImages([imagePath, outputImage, tempTextImage]);
        return response.data.data;
    } catch (error) {
        console.error(error);
        throw new Error(error?.response?.data?.error?.message || error.message);
    }
};
