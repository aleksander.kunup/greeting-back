# turings-henchmen-AI-hackathon
This project provides an API for generating personalized greetings and images using OpenAI GPT-3 API. It also provides a web interface for interacting with the API.

## Project Setup


### Install dependencies
```sh
npm install
```
### Lint with [ESLint](https://eslint.org/)
```sh
npm run eslint
```

### Start and Hot-Reload for Development
```sh
npm run dev
```

### Start the server
```sh
npm start
```

### Open Swagger interface in your browser
```sh
http://localhost:3000/api-docs
```

